// Global variables
float scale;
int iterations = 1000;
float zoom = 1, clickX, clickY;
int clicks = 0;
int row = 0;
int rowsPerIteration = 10;
// Setup the Processing Canvas
void setup(){
  size( 600, 600 );
  clickX = width / 2;
  clickY = height / 2;  
  scale = width/3;
}

// Main draw loop
void draw(){
  loadPixels();
  for(int i = 0; i < rowsPerIteration && row + i < height; i++){
    for(int j = 0; j < height; j++){
      double x = ((j + zoom*clickX-width/2)/zoom-width/2)/scale;
      double y = ((row+i + zoom*clickY-height/2)/zoom-height/2)/scale;
      int iter = mandelbrotCheck(x, y, iterations);
      if(iter == iterations){
        pixels[j+width*(row+i)] = color(0,0,0);
      }
      else{
        float intensity = lerp(0, 200, ((float) iter)/200);
        pixels[j+width*(row+i)] = color(intensity,0,max(0,10*clicks-intensity));
      }
    }
  }
  row += rowsPerIteration;
  updatePixels();
  if(row >= height)
    noLoop();   
}

void mouseClicked() {
  //println("old: ", clickX, clickY," clicked: ", mouseX, mouseY);
  clickX =(clickX - (clickX - mouseX)/zoom);
  clickY =(clickY - (clickY - mouseY)/zoom);
  //println("new: ", clickX, clickY);
  if(mouseButton == LEFT) {
    zoom *= 2;
    clicks++;
  }
  if(mouseButton == RIGHT){
    zoom /= 2;
    clicks--;
  }
  row = 0;
  loop();
}

//z^2+x+yi
int mandelbrotCheck(double x, double y, int iterations){  
  double startX = x;
  double startY = y;
  for(int i = 0; i < iterations; i++){
    //(x+yi)^2 = x^2 - y^2 + 2xyi
    double oldX = x;
    double xsqr = x*x;
    double ysqr = y*y;
    x = xsqr - ysqr + startX;
    y = (oldX+oldX)*y + startY;
    if(xsqr+ysqr > 5) return i;
  }
  return iterations;  
}